
F.train_memory= {
    store = {} -- line -> train_id -> location
    }

F.split = function (str, delimiter)
-- an useful function to split a string with delimiter. Thanks to flux for the code.
    local rv = {}
    local i, j = str:find(delimiter, nil, true)
    while i do
        table.insert(rv, str:sub(1, i - 1))
        str = str:sub(j + 1)
        i, j = str:find(delimiter, nil, true)
    end
    table.insert(rv, str)
    return rv
end


F.train_memory.get_location = function(line, train_id)
    local train_store = F.train_memory.store[line]
    if not train_store then
      train_store = {}
      F.train_memory.store[line] = train_store
    end

    return train_store[train_id]
  end

  F.train_memory.set_location = function(line, train_id, location)
    local train_store = F.train_memory.store[line]
    if not train_store then
      train_store = {}
      F.train_memory.store[line] = train_store
    end

    train_store[train_id] = location
  end
-- String storing formatF.train_memory.store[line][train_id] = "at/left/arriving:station:destination"
